function getRandomInt(min, max) {
    // range: [min; max)
    return (Math.floor(Math.random() * (max - min)) + min);
}

class ArrowDance {
	constructor() {
		this.arrows;
		this.responseTime;
		this.result;
		this.response;
		this.timer;
		this.randomArrow;
		this.play;
		this.backlight;
	}

	init = () => {
		this.arrows = document.querySelectorAll(".dance .arrow");
		this.responseTime = 2000;	// ms
		this.result = 0;
		this.response;
		this.backlight = false;
		this.play = false;
	}

	static exceptionColor = [15, 235];	// not black, not white

	startGame = (e) => {
		this.play = !this.play;
		this.dance();
	}

	dance = () => {
		this.randomArrow = this.getRandomArrow();
		this.toggleBacklight();
		this.setTimer();
	}

	getRandomArrow = () => {
		return this.arrows[getRandomInt(0, this.arrows.length)];
	}

	toggleBacklight = () => {
		this.backlight = !this.backlight;

		if (this.backlight) {
			let r, g, b;

			do {
				r = getRandomInt(0, 255);
				g = getRandomInt(0, 255);
				b = getRandomInt(0, 255);
			} while (
				(
					r <= ArrowDance.exceptionColor[0] &&
					g <= ArrowDance.exceptionColor[0] &&
					b <= ArrowDance.exceptionColor[0]
				) ||
				(
					r >= ArrowDance.exceptionColor[1] &&
					g >= ArrowDance.exceptionColor[1] &&
					b >= ArrowDance.exceptionColor[1]
				)
			);

			this.randomArrow.style.backgroundColor = `rgb(${r}, ${g}, ${b})`;
		} else {
			this.randomArrow.style.backgroundColor = "";	
		}
	}

	setTimer = () => {
		this.timer = setTimeout(() => {
			this.setLost();
		}, this.responseTime);
	}

	stopTimer = () => {
		clearTimeout(this.timer);
	}

	setResponse = (e) => {
		if (!this.play) {
			return;
		}

		this.response = document.querySelector(`.arrow[data-keys='${e.which}']`);

		if (this.response) {
			this.nextStep();
		}
	}

	nextStep = () => {
		if (this.checkResponse()) {
			this.stopTimer();
			this.toggleBacklight();
			this.result++;
			this.dance();
		} else {
			this.setLost();
		}
	}

	checkResponse = () => {
		return this.response === this.randomArrow;
	}

	setLost = () => {
		this.stopTimer();
		this.play = false;

		const restart = confirm(`You lose, result: ${this.result}.\nRestart?`);
		if (restart) {
			this.restart();
		} else {
			this.reset();
		}
	}

	restart = () => {
		this.reset();
		this.dance();
		this.play = true;
	}

	reset = () => {
		this.stopTimer();
		this.toggleBacklight();
		this.result = 0;
	}
}

const arrowDance = new ArrowDance;
arrowDance.init()

const play = document.getElementById("play");
play.addEventListener("click", arrowDance.startGame);

document.body.addEventListener("keydown", arrowDance.setResponse);
